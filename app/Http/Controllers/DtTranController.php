<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\model\tblcoa;
use App\model\smTran;
use App\Models\dtTran;

class DtTranController extends Controller
{
    /**
     * @OA\Get(
     *      path="/dtTran",
     *      summary="Get public dtTran",
     *      @OA\Parameter(name="page",
     *          in="query",
     *          required=false,
     *          @OA\Schema(type="number")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *      )
     * )
     */
    public function showAllDatas()
    {
        $dtTran = new dtTran;
        return response()->json([
            'response' => [
                'status'=>200,
                'message' => "OK",
                'data' => $dtTran->getAll()
            ]
        ],200);
    }

    public function showAllData($noBatch)
    {
        return response()->json(dtTran::find($noBatch));
    }

    public function store(Request $request ,$noBatch)
    {   

        $journalData = smTran::select('noBatch', 'prdbln', 'prdthn', 'keterangan', 'tgl_proses')
                                ->where('noBatch', $noBatch)
                                ->first();

        $coaCode = $request->input('coa-code');
        $description = $request->input('description');
        $debitOrCredit = $request->input('debit-or-credit');
        $value = $request->input('value');

        $journalNoUrutData = tblcoa::select('nourut')
                                ->where('kodeCoa', $coaCode)
                                ->first();

        if ($debitOrCredit == 'K') {
            $value = -($value);
        }


        $journalDetail = new dtTran;
        $journalDetail->noBatch = $journalData['noBatch'];
        $journalDetail->prdbln = $journalData['prdbln'];
        $journalDetail->prdthn = $journalData['prdthn'];
        $journalDetail->nourut = $journalNoUrutData['nourut'];
        $journalDetail->kodeCoa = $coaCode;
        $journalDetail->keterangan = $description;
        $journalDetail->dk = $debitOrCredit;
        $journalDetail->amount = $value;
        $journalDetail->tgl_proses = $journalData['tgl_proses'];
        $journalDetail->tgl_validasi = now();

        try {
            DB::beginTransaction();

            $journalDetail->save();

            DB::commit();
        } catch(\Exception $ex) {
            DB::rollBack();
            return response(['message' => $ex->getMessage()], 500);

        } catch(\Throwable $ex) {
            DB::rollBack();
            return response(['message' => $ex->getMessage()], 500);
        }

        $response = [
            'message' => [
                'icon' => 'success',
                'title' => 'Jurnal Tersimpan',
                'text' => 'Jurnal Detail telah dibuat'
            ],
            'code' => 200
        ];

        return response($response, $response['code']);
    
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            dtTran::where('id', $id)
                    ->delete();

            DB::commit();
        } catch(\Exception $ex) {
            DB::rollBack();
            return response(['message' => $ex->getMessage()], 500);

        } catch(\Throwable $ex) {
            DB::rollBack();
            return response(['message' => $ex->getMessage()], 500);

        }

        $response = [
            'message' => [
                'icon' => 'success',
                'title' => 'Jurnal Terhapus',
                'text' => 'Jurnal berhasil dihapus'
            ],
            'code' => 200
        ];

        return response($response, $response['code']);
    }
}
