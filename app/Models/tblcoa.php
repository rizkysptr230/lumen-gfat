<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class tblcoa extends Model
{
    protected $table = "tblcoa";
    protected $primaryKey = 'kodeCoa';
    protected $fillable = [
        'kodeCoa', 'nourut', 'keterangan', 'isActive', 'tgl_proses', 'userId', 'updated_at', 'created_at'
    ];

    public function getAllData(){
        $data=tblcoa::where('nourut', '4')->get();
        // dd($data);
        return $data;
    }
    public function getDataByKode($kode){
        $data=tblcoa::where('kodeCoa',$kode)
        ->first();
        return $data;
    }
    public function insertData($query){
        $data= tblcoa::newInstance($query);
        $data->userId = Auth::user()->idUser;
        $data->save();
        return $data;
    }
    public function updateDataByKode($kode,$update){
        $data = tblcoa::where('kodeCoa',$kode)
                        ->first();
        $data->kodeCoa= $update['kodeCoa'];
        $data->nourut=$update['nourut'];
        $data->keterangan=$update['keterangan'];
        $data->isActive=$update['isActive'];
        $data->userId = Auth::user()->idUser;
        $data->save();
    }

    public function KodeCoa()
    {
        return $this->hasMany('App\model\dtTran');
    }

    public function groupOfProfit()
    {
        return $this->belongsTo('App\model\profit_group', 'idGroup', 'idGroup');
    }

}
