<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class dtTran extends Model
{
    public $timestamps = false;

    protected $table = 'dt_tran';
    protected $fillable = [];

    const CREATED_AT = 'tgl_validasi';

    public function coaName()
    {
        return $this->belongsTo('App\model\tblcoa', 'kodeCoa', 'kodeCoa');
    }

    public function profitGroup()
    {
        return $this->hasMany('App\model\profit_group');
    }
    public function getAll(){
        $data = dtTran::get();
        return $data;
    }



    // function filter($bulan, $tahun){
    //     $data = dtTran::select(
    //         '*',
    //         //DB::raw('sum(amount) as mutasi')
    //     )
    //     ->where('prdbln', $bulan)
    //     ->where('prdthn', $tahun)
    //     //->groupBy('kodeCoa')
    //     ->get();
    //     return $data;
    // }
    
    
}
