<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class smTran extends Model
{
    public $timestamps = false;

    protected $table = 'sm_tran';
    protected $primaryKey = 'noBatch';
    protected $fillable = [];

    const CREATED_AT = 'tgl_update';

}
